<!doctype html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <title>Maintenance - Pemerintah Provinsi Sulawesi Utara</title>
	    <meta name="description" content="Responsive HTML5 Template" />
	    <meta name="keywords" content="Onepage, creative, modern, bootstrap 5, multipurpose, clean" />
	    <meta name="author" content="Shreethemes" />
	    <meta name="website" content="https://shreethemes.in" />
	    <meta name="email" content="support@shreethemes.in" />
	    <meta name="version" content="1.0.0" />
	    <!-- favicon -->
        <link href="images/favicon.ico" rel="shortcut icon">
		<!-- Bootstrap core CSS -->
	    <link href="css/bootstrap.min.css" type="text/css" rel="stylesheet" />
        <link href="css/tobii.min.css" rel="stylesheet" type="text/css" />
        <link href="css/materialdesignicons.min.css" rel="stylesheet" type="text/css" />
        <link href="https://unicons.iconscout.com/release/v4.0.0/css/line.css" rel="stylesheet">
	    <!-- Custom  Css -->
	    <link href="css/style.min.css" rel="stylesheet" type="text/css" id="theme-opt" />
	</head>

	<body onload="run();" class="rainday-css">
        <div>
            <img id="background" class="rain-img" alt="background" src="images/bg/4.jpg" />
        </div>

        
        <!-- Start -->
        <section class="position-relative z-index-100">
            <div class="bg-overlay bg-linear-gradient-2"></div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 p-0">
                        <div class="d-flex flex-column min-vh-100 justify-content-center px-md-5 py-5 px-4">
                            <div class="text-center">
                                <a href=""><img src="images/logo ver hitam.png" alt="" height="60px"></a>
                            </div>
                            <div class="title-heading text-center my-auto">
                                <div class="wave-effect coming-soon fw-bold display-3 text-white">
                                    <span style="--a:1">M</span>
                                    <span style="--a:2">A</span>
                                    <span style="--a:3">I</span>
                                    <span style="--a:4">N</span>
                                    <span style="--a:5">T</span>
                                    <span style="--a:6">E</span>
                                    <span style="--a:7">N</span>
                                    <span style="--a:8">A</span>
                                    <span style="--a:9">N</span>
                                    <span style="--a:10">C</span>
                                    <span style="--a:11">E</span>
                                </div>
                                
                                <p class="text-white para-desc mx-auto mt-2 mb-0">Website yang anda kunjungi sedang dalam masa pemeliharaan.<br>Mohon maaf atas ketidaknyamanannya...</p>
            
                                
                            </div>
                            <div class="text-center footer">
                                <p class="mb-0 text-reset">© <script>document.write(new Date().getFullYear())</script> Maintenance. Created by <a href="#" class="text-reset">DKIPSD SULUT</a>.</p>
                            </div>
                        </div>
                    </div><!--end col-->
                </div><!--end row-->
            </div><!--end container-->
        </section><!--end section-->
        <!-- End -->

       

        <!-- JAVASCRIPTS -->
	    <script src="js/bootstrap.bundle.min.js"></script>
	    <script src="js/rainyday.min.js"></script>
	    <script src="js/tobii.min.js"></script>
	    <script src="js/contact.js"></script>
        <script src="js/feather.min.js"></script>
	    <!-- Custom -->
	    <script src="js/app.js"></script>
        <script>
			function run() {
				var image = document.getElementById('background');
				image.onload = function () {
					var engine = new RainyDay({
						image: this
					});
					engine.rain([
						[0, 2, 200],
						[3, 3, 1]
					], 100);
				};
				image.crossOrigin = 'anonymous';
				// image.src = 'http://i.imgur.com/OVQkDRE.jpg';
			}

		</script>
    </body>
</html>